from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range)

from django.utils.translation import ugettext as _
from django.conf import settings

from itertools import product
from math import ceil, floor

import random
import json

author = 'Steffen Ahrens, Ciril Bosch, and Rasmus Pank Roulund'

doc = 'Dot-Spot example'
## Utility functions.

def dotspot_gen_data(nred=None, dim=20, error = None):
    """Generate dotspot dataset.

    Parameters
    ----------
    nred:
        number of red dots in data
    dim:
        total number of dots per index.  Dimensions are
    error:
        added to nred, nred ± error  ,  error ∈ [-sd, sd]

    Returns
    -------
    list
        list of dictonaries with the keys x, y and color.
    """
    total = dim * dim
    if nred is None:
        half = total //  2
        nred = half
    if error:
        nred += random.randint(-error, error)
    colors = ["red"]*nred + ["blue"]*(total - nred)
    random.shuffle(colors)
    base = product(range(dim), range(dim))
    data = [{"x": x, "y": y, "color": col} for (x, y), col in zip(base, colors)]
    return(data)

def reward(guess, correct,
           intervals_pct=(.1, .25, .5),
           rewards=(c(25), c(10), c(5), c(0))):
    """
    Calculate a reward from distance between `guess` and `correct`
    """
    intervals_pct_ = ((1-x, 1+x) for x in intervals_pct)
    intervals = ((correct * part for part in elm) for elm in intervals_pct_)
    intervals = ((floor(l), ceil(u)) for l, u in intervals)
    true_intervals = [True if interval[0] <= guess <= interval[1] else False
                          for interval in intervals]
    ## Make sure that there is at least one True
    true_intervals += [True]
    return rewards[true_intervals.index(True)]

## Start of main oTree classes.

class Constants(BaseConstants):
    name_in_url = 'dotspot'
    num_dotspots_at_time = 5
    dotspot_nred = [(45, 5),  (75, 5),  (325, 5)] + [(200, 125)]*2
    dotspot_seconds = settings.DOTSPOT_SECONDS
    dotspot_dim = 20
    num_rounds = len(dotspot_nred)
    players_per_group = None

class Subsession(BaseSubsession):

    def prepare_dotspot_data(self):
        """Generate dotspot data.

        Note, these will be popped, so the actual order will be reversed.
        """
        ## All data is being generated at once as we want to randomize the order.
        if self.round_number == 1:
            dotspots = [dotspot_gen_data(reds, dim=Constants.dotspot_dim, error=eps)
                        for (reds, eps) in Constants.dotspot_nred]
            random.shuffle(dotspots)
            colors = [[item["color"] for item in dotspot] for dotspot in dotspots]
            dist = [{"red": color.count("red"), "blue": color.count("blue")} for color in colors]
            self.session.vars["dotspot_dist"] =  dist
            self.session.vars["dotspot_reds_series"] = [x["red"] for x in dist]
            self.session.vars["dotspot_data_series"] = dotspots

class Group(BaseGroup):
    ## Dotspots information
    dotspot_dist_str = models.StringField()
    dotspot_reds = models.IntegerField()
    dotspot_data_str = models.LongStringField()
    dotspot_group = models.IntegerField(initial=0)

    def set_dotspot_data(self):
        """Pop dotspot data if in dotspot round."""
        s = self.subsession.session
        r = self.round_number - 1
        self.dotspot_dist_str = json.dumps(s.vars["dotspot_dist"][r])
        self.dotspot_data_str = json.dumps(s.vars["dotspot_data_series"][r])
        self.dotspot_reds = s.vars["dotspot_reds_series"][r]

    def dotspot_results(self):
        for p in self.get_players():
            p.dotspot_correct = self.dotspot_reds
            p.dotspot_realized_error = abs(p.dotspot_guess - p.dotspot_correct)
            p.dotspot_guess_earnings = reward(p.dotspot_guess, p.dotspot_correct)
            p.dotspot_error_earnings = reward(p.dotspot_expected_error, p.dotspot_realized_error)
            p.dotspot_round_earnings_from = random.choice(("guess", "error"))
            p.dotspot_round_earnings = getattr(p, f"dotspot_{p.dotspot_round_earnings_from}_earnings")
            p.payoff += c(p.dotspot_round_earnings)

class Player(BasePlayer):
    dotspot_guess = models.IntegerField(
        initial=None,
        min = 0,
        label=_("Please estimate the total number of red dots in the graph"))

    dotspot_correct = models.IntegerField(
        initial=None,
        min = 0,
        label=_("How many *RED* dots were there in the matrix?"))

    dotspot_expected_error = models.IntegerField(
        initial=None,
        min = 0,
        label=_("""How far away do you think your estimate is from the correct answer?"""))

    dotspot_realized_error = models.IntegerField(
        initial=None,
        label=_("Difference between correct and guess"))

    dotspot_guess_earnings = models.CurrencyField(
        initial=None,
        label = _("Realized earnings from red dots guess and no. red dots"))

    dotspot_error_earnings = models.CurrencyField(
        initial=None,
        label = _("Realized earnings from guessed vs reaalized errors"))

    dotspot_round_earnings = models.CurrencyField(
        initial=None,
        label = _("Earnings from dotspot within round"))

    dotspot_round_earnings_from = models.StringField(
        initial = "",
        label = _("'error' or 'guess', depending on which activity is rewarded"))
