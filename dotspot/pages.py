from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from django.utils.translation import ugettext as _
from .models import Constants
from django.conf import settings

import json

def debug_mode(s):
    return s.session.config["debug"] if "debug" in s.session.config else settings.DEBUG

def vars_for_all_templates(self):
    return {'info': "",
             "debug_mode": debug_mode(self),
             'ndots': Constants.dotspot_dim**2}

class PrepareNewRound(WaitPage):
    """Set up values for new round."""
    def after_all_players_arrive(self):
        self.subsession.prepare_dotspot_data()
        for group in self.subsession.get_groups():
            group.set_dotspot_data()

## DOTSPOT PAGES

class DotSpotIntro(Page):
    """
    Page showing DotSpot introductions.

    Only shown for the first dotspot page.
    """

    def vars_for_template(self):
        return {"sec": Constants.dotspot_seconds}

    def is_displayed(self):
        return self.round_number == 1

class DotSpotStart(Page):
    """Page to click continue before seeing dotspot image"""

    template_name = "dotspot/DotSpotImage.html"

    def vars_for_template(self):
        return {"data": '[]',
                "info": _('Click "Next" to show the DotSpot image.')}

class DotSpotImage(Page):
    """Page for showing the dotspot image.

    The image is flashed for some seconds.
    """

    form_model = "player"
    timeout_seconds = Constants.dotspot_seconds

    def vars_for_template(self):
        return {"data": json.loads(self.group.dotspot_data_str)}

class DotSpotPrediction(Page):
    """Page to guess number of dots"""

    form_model = "player"
    # timeout_seconds = 60
    form_fields = ['dotspot_guess', 'dotspot_expected_error']

    def dotspot_guess_max(self):
        return Constants.dotspot_dim**2

    def dotspot_expected_error_max(self):
        return Constants.dotspot_dim**2


class DotSpotResults(WaitPage):
    "Calculate results for DotSpot round"
    wait_for_all_groups = True

    def after_all_players_arrive(self):
        "First compute the fields for players and then add rewards."
        for group in self.subsession.get_groups():
            group.dotspot_results()

class DotSpotCheck(Page):
    "Calculate results for DotSpot round"
    def is_displayed(self):
        return debug_mode(self)

class Result(Page):
    """This page shows the final earnings across all markets."""

    def is_displayed(self):
        """Only display at the end of a market"""
        return self.round_number == Constants.num_rounds

    def vars_for_template(self):
        return {"payoff": self.participant.payoff}

page_sequence = [
    PrepareNewRound,
    DotSpotIntro,
    DotSpotStart,
    DotSpotImage,
    DotSpotPrediction,
    DotSpotResults,
    DotSpotCheck,
    Result]
